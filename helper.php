<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$doc = JFactory::getDocument();
$doc->addStyleSheet('modules/mod_rokoho_contact_map/css/mod_rokoho_contact_map.css');
$doc->addScript('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language=nl');

class modContactMapHelper
{

	public function getContactDetails($ids) {

		$id_array = explode(',', $ids);

		$db = JFactory::getDbo();

		foreach ($id_array as $key => $id) {

			// Create a new query object.
			$query = $db->getQuery(true);


			$query->select($db->quoteName(array('name', 'con_position', 'address', 'suburb', 'state', 'country', 'postcode', 'image', 'telephone', 'mobile')));
			$query->from($db->quoteName('#__contact_details'));
			$query->where($db->quoteName('id')." = ". $id);
			$db->setQuery($query);
			$row[] = $db->loadAssoc();
		}		 

		return $row;
	}
}

?>